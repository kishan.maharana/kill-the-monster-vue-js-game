new Vue({
    el: "#app",
    data: {
        userHealth: 100,
        monsterHealth: 100,
        running: false,
        lidata1: [],
        lidata2: [],
        i: -1,
        li: false,
    },
    methods: {
        start: function() {
            this.running = true;
        },
        attack: function() {
            this.li = true;
            var max = Math.floor(Math.random() * 10) + 1;
            var min = 3;
            var final = Math.max(max, min);
            this.lidata1.push(final);
            this.i++;
            this.userHealth -= final;
            if (this.userHealth <= 0) {
                this.userHealth = 0;
                alert("monster wins the match");
                this.userHealth = 100;
                this.monsterHealth = 100;
                this.running = false;
                this.li = false;
                return;
            }
            var maxm = Math.floor(Math.random() * 10) + 1;
            var minm = 3;
            var finalm = Math.max(maxm, minm);
            this.lidata1.push(finalm);
            this.i++;
            this.monsterHealth -= finalm;
            if (this.monsterHealth <= 0) {
                this.monsterHealth = 0;
                alert("you wins the match");
                this.monsterHealth = 100;
                this.userHealth = 100;
                this.running = false;
                this.li = false;
                return;
            }
        },
        spattack: function() {
            this.li = true;
            var max = Math.floor(Math.random() * 10) + 1;
            var min = 6;
            var final = Math.max(max, min);
            this.lidata1.push(final);
            this.i++;
            this.userHealth -= final;
            if (this.userHealth <= 0) {
                this.userHealth = 0;
                alert("you wins the match");
                this.userHealth = 100;
                this.monsterHealth = 100;
                this.running = false;
                this.li = false;
                return;
            }
            var maxm = Math.floor(Math.random() * 10) + 1;
            var minm = 6;
            var finalm = Math.max(maxm, minm);
            this.lidata1.push(final);
            this.i++;
            this.monsterHealth -= finalm;
            if (this.monsterHealth <= 0) {
                this.monsterHealth = 0;
                alert("you wins the match");
                this.monsterHealth = 100;
                this.userHealth = 100;
                this.running = false;
                this.li = false;
                return;
            }

        },
        heal: function() {
            var healAmount = 20;
            if (this.userHealth >= 100 || this.monsterHealth >= 100) {
                this.userHealth = 100;
                this.monsterHealth = 100;
                return;
            } else {
                this.userHealth += healAmount;
                this.monsterHealth += healAmount;
                if (this.userHealth >= 100 || this.monsterHealth >= 100) {
                    this.userHealth = 100;
                    this.monsterHealth = 100;
                    return;
                }
            }
        },
        giveup: function() {
            this.running = false;
            this.userHealth = 100;
            this.monsterHealth = 100;
            this.li = false;
        }
    }
});